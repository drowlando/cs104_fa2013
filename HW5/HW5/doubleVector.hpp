#ifndef _DOUBLE_VECTOR
#define _DOUBLE_VECTOR

#include <iostream>
#include <stdexcept>
#include <string>



using namespace std;


class doubleVector
{
private:
	int dimensions;// the number of dimensions the vector has
	double* values; // pointer to a dynamically allocated array of doubles
	
	

public:
	/*double* getValues();*/
	doubleVector(void);

	doubleVector(string vectorString) throw (invalid_argument);

	doubleVector(doubleVector& aVector);

	doubleVector(double number);

	~doubleVector(void);

	string toString();

	bool operator==(doubleVector const & other);

	doubleVector  operator+(doubleVector& other) throw (logic_error);

	doubleVector  operator-(doubleVector& other) throw (logic_error);

	doubleVector  operator*(doubleVector& other) throw (logic_error);

	doubleVector & operator=(doubleVector& other);

};

#endif

namespace helper
{
	string doubleFormat(double number);
	int countRecurrence(string line, char c);
	void vectorStringParce(string line, double a[], int index);
	double innerProduct(double a[], double b[], int arraySize);
	void scalarMult(double empty[], double full[], double scalar,int aSize);
	
}
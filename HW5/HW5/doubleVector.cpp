#include "doubleVector.hpp"
#include <iostream>
#include <string>
#include <cstring>

using namespace std;
using namespace helper;


doubleVector::doubleVector(void): dimensions(0), values(nullptr)
{
}


doubleVector::~doubleVector(void)
{
	delete[] values;
	values = nullptr;

}

doubleVector::doubleVector(doubleVector& aVector){
	dimensions = aVector.dimensions;
	values = new double[dimensions];
	for (int i = 0; i < dimensions; i++){
		values[i] = aVector.values[i];
	}
}

doubleVector::doubleVector(double number){
	dimensions = 1;
	values = new double[1];
	values[0] = number;
}





doubleVector::doubleVector(string vectorString) throw(invalid_argument) {
	
	int commas = countRecurrence(vectorString,',');
	
	int spaces = countRecurrence(vectorString, ' ');
	
	bool validEntry = ((vectorString.front() == '[') || (vectorString!="[]") || (spaces == 0) || (vectorString.back() == ']') );
	
	if(!validEntry ) throw invalid_argument("Format of vector not in [d0,d1,...,dn-1,dn] format");
	
	dimensions = commas+1; 
	
	values = new double[dimensions];

	vectorStringParce(vectorString.substr(1),values,0);

	cout << "in constructor test" << endl;
	for(int i = 0; i<3;i++){
		cout << values[i] << endl;
	}
	
}






int helper::countRecurrence(string line, char c){
	int count = 0;
	for(int i = 0; i < line.size(); i++){
		if (line.at(i) == c) count++;
	}
	return count;
}

string doubleVector::toString(){
	string temp = "[";
	if(dimensions == 1) return temp + doubleFormat(values[0]) + "]";
	else{
		temp += doubleFormat(values[0]);
		for (int i = 1; i<dimensions; i++){
			
			temp += "," + doubleFormat(values[i]);
			
		}
	}
	temp += "]";
	return temp;
}

string helper::doubleFormat(double number){
	string temp = std::to_string(number);
	int decimalLocation = temp.find(".");

	return temp.substr(0,decimalLocation +3);

}

doubleVector doubleVector::operator+(doubleVector& other) throw(logic_error){
	if(this->dimensions != other.dimensions) throw logic_error("You cannot add two vectors with different # of dimensions");
	
	
	doubleVector temp;
	temp.dimensions= this->dimensions;
	temp.values = new double[dimensions];
	for(int i = 0; i<dimensions;i++){
		temp.values[i]= this->values[i]+other.values[i];

	}
	return temp;
}

doubleVector doubleVector::operator-(doubleVector& other) throw(logic_error){
	if(this->dimensions != other.dimensions) throw logic_error("You cannot add two vectors with different # of dimensions");
	
	
	doubleVector temp;
	temp.dimensions= this->dimensions;
	temp.values = new double[dimensions];
	for(int i = 0; i<dimensions;i++){
		temp.values[i]= this->values[i] - other.values[i];

	}
	return temp;
}

doubleVector & doubleVector::operator=(doubleVector& other){
	if (this == &other) return *this;
	this->dimensions= other.dimensions;
	delete this->values;
	this->values = new double[other.dimensions];
	for(int i = 0; i < other.dimensions; i++){
		this->values[i] = other.values[i];

	}
	return *this;
}

bool doubleVector::operator==(doubleVector const & other){
	if(this->dimensions != other.dimensions) return false;
	else{
		for(int i = 0; i<dimensions ; i++){
			if(this->values[i] != other.values[i]){
				return false;
			}
		}
	}
	return true;
}

void helper::vectorStringParce(string line, double a[], int index){
	int end = line.find_first_of(',');
	if(end==-1){
		
		a[index] = stod(line.substr(0,line.size()-1));
	}else{

		a[index] = stod(line.substr(0,end));
		vectorStringParce(line.substr(end+1,line.size()-1), a, index+1);
		return;
	}
	
	return;


	
}

doubleVector  doubleVector::operator*(doubleVector& other) throw (logic_error){
	if((this->dimensions != other.dimensions) && (this->dimensions > 1) && (other.dimensions>1)) throw logic_error("You cannot add two vectors with different # of dimensions");
	doubleVector temp ;
	if (this->dimensions == other.dimensions){
		temp.dimensions=1;
		temp.values = new double[1];
		temp.values[0] = innerProduct(this->values,other.values,other.dimensions);
		return temp;
	}
	else{
		double scalar;
		if (this->dimensions == 1){ 
			scalar = this->values[0];
			temp.dimensions= other.dimensions;
			temp.values =  new double[temp.dimensions];
			scalarMult(temp.values,other.values,scalar, temp.dimensions);
		}else{
			scalar = other.values[0];
			temp.dimensions = this->dimensions;
			temp.values =  new double[temp.dimensions];
			scalarMult(temp.values,this->values,scalar, temp.dimensions);
		}
	}


	return temp;

}
double helper::innerProduct(double a[], double b[], int arraySize){
	double temp = 0.0;
	for (int i = 0; i< arraySize;i++){
		temp += (a[i]*b[i]);
	}
	return temp;
}

void helper::scalarMult(double empty[], double full[],double scalar, int aSize){
	for( int i = 0; i<aSize; i++){
		empty[i] = full[i]*scalar;
	}
}

int main(){

	string test = "[0.57,-1.414,3.14159]";
	string testA = "[1.25,2.00,3.75]";
	string testB = "[2.75,2.00,0.25]";
	string testC = "[1.00,2.00,3.00,4.00]";
	doubleVector a(4.0);

	//test helper function doubleFormat 
	double nb = -189.0;
	cout << doubleFormat(nb) << endl;


	//test helper function countRecurrence function
	int z = countRecurrence(test, ',');
	cout << "Should be 2: " << z << endl;

	//test helper function vectorStringParce

	double doub[3];
	vectorStringParce(test.substr(1),doub, 0);

	cout << "The contents of the double array are: " << endl;

	for(int i = 0; i<3;i++){
		cout << doub[i] << endl;
	}

	// test string constr
	cout << "//////////////////////////"<<endl;
	cout << "test string argument doubleVector constructor"<<endl;
	doubleVector v = doubleVector(test);
	doubleVector w = doubleVector(testA);
	doubleVector x = doubleVector(testB);
	doubleVector y = doubleVector(testB);
	cout << v.toString() << endl;
	cout << w.toString() << endl;
	cout << x.toString() << endl;

		// test == operator overload
	cout << "testing == operator overload\n" << endl;
	doubleVector b(4.0);
	doubleVector c(4.1);
	cout <<"Should be true: " << (a==b) << endl;
	cout <<"Should be false: " << (a==c) << endl;
	cout << "Should be false: " << (v==x) << endl;
	cout << "Should be true: " << (x==y) << endl;

	cout << "testing +, - and = operator overload\n" << endl;
	doubleVector wPlusX = w + x;
	cout << wPlusX.toString() << endl;
	try{
		wPlusX = w + b;
	}catch(logic_error){
		cout << "logic error exception works"<< endl;
	}
	wPlusX = w - x;
	cout << wPlusX.toString() << endl;

	cout << endl;
	cout << "testing the * operator overload" << cout;

	doubleVector factor1 = doubleVector(testC);
	doubleVector factor2 = doubleVector(4);
	cout<< factor1.toString() << " * " << factor2.toString() << "= " << (factor1*factor2).toString()<< endl;
	cout<< factor1.toString() << " * " << factor1.toString() << "= " << (factor1*factor1).toString()<< endl;


	return 0;
}
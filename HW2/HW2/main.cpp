#include<iostream>
#include<cmath>
using namespace std;


// addArray function takes an integer array and adds the elements recursively, 
// using int i to keep track of the levels of recursion. Intitial i value should be 0. 
// Note: Function taken from Homework 1. 
int addArray(int array[],int arraySize, int i)
{
	if (i>=arraySize){
		return 0;
 	}
 	int numb = array[i];
 	i+=1;
 	return numb + addArray(array,arraySize,i);
 }

// nearestNum compares an integer value, currentSide, to the contents of an 3 element array and determins which element is closest in value
int nearestValidNum(int array[3], int validMoves[20][3], int currentSide)
{
	int difference; 
	int lowestValidNextStep;
	int validMoveIndex;
	// if else cascade creates an initial value for difference and 
	// lowestValidNextStep that will me used as the base comparison in the for loop below
	if (validMoves[currentSide-1][0] == 0){

	difference = abs(currentSide - array[0]);
	lowestValidNextStep = array[0];
	validMoveIndex = 0;
		}else{
			if(validMoves[currentSide-1][1] == 0){
				difference = abs(currentSide - array[1]);
				lowestValidNextStep = array[1];
				validMoveIndex = 1;
			}else{
					if(validMoves[currentSide-1][2]==0){
					difference = abs(currentSide - array[2]);
					lowestValidNextStep = array[2];
					validMoveIndex = 2;
					}
			
				//return -1; // if none of the sides are valid options return -1 as a sentinal value.
				}
	}
	// loop throught the contents of the array, if the value is closer to that of currentSide 
	// and it is a valid (unvisited) move, then it becomes the LowestValidNextStep!!!
	for (int i = 0; i<3;i++){
		if ((abs(currentSide - array[i])< difference) && (validMoves[currentSide-1][i]==0)){
			//if lower AND isVALIDMove THEN CHANGE IT!!! weeee
			difference = abs(currentSide - array[i]);
			lowestValidNextStep = array[i];
			validMoveIndex = i;
		}

	}
	validMoves[currentSide-1][validMoveIndex]=1;//if backtracking occurs, make sure the same path isn't chosen again. 
 	return lowestValidNextStep;
 }

 void resetArray(int array[3]){
 	for(int i = 0; i<3; i++){
 		array[i] = 0;
 	}
 }

int traverse(int array[20][3],int sidesVisited[], int validMoves[20][3], int currentSide,  int lastMove, int antEnergy){
	sidesVisited[currentSide-1]=1;
	cout << currentSide << endl;
	//cout << addArray(sidesVisited,20,0) << endl;
	if (addArray(sidesVisited,20,0) == 20){// Base case 1. if all of the sides have been visited
		cout << "base case #1";
		antEnergy += abs(lastMove - currentSide);
		return antEnergy;
	}else{if(sidesVisited[array[currentSide-1][0]] + sidesVisited[array[currentSide-1][1]] + sidesVisited[array[currentSide-1][2]] == 3){ //if all of the adjacent sides have been visited. 
			cout << addArray(sidesVisited,20,0) << endl;;
			cout << "deadend reached"<< endl;
			resetArray(validMoves[currentSide-1]);
			sidesVisited[currentSide-1] = 0; // go back the way you came and erase tracks on the way out
			//validMoves[lastMove-1][nextMoveIndex] = 1;//make sure you don't come this way again, from the last side.
			resetArray(validMoves[currentSide-1]);

			return 0;// 0 is a sentinal value
			}

		}
	int tempCurrentSide = nearestValidNum(array[currentSide-1],validMoves,currentSide);
	//cout << tempCurrentSide << endl;
	// if (tempCurrentSide == -1){
	// 	return 0; // 0 is a sentinal value.
	// } 
	lastMove = currentSide;
	currentSide = tempCurrentSide;
	int tempAntEnergy = traverse(array,sidesVisited,validMoves, currentSide, lastMove, antEnergy);
	 if((tempAntEnergy == 0)&& ((sidesVisited[array[currentSide-1][0]] + sidesVisited[array[currentSide-1][1]] + sidesVisited[array[currentSide-1][2]] == 3))){ // if the road ahead is impassable, go back, but first open up the adjoining paths for another try from another side. 
	 	cout<< "I actually went in here"<< endl;
	 	resetArray(validMoves[currentSide-1]);
	 	return 0;
	 }
	return antEnergy + tempAntEnergy;

	}

int main(){

//	int antEnergy; // the total energy used by an ant traversing the 20 sided die.



	int die[20][3] = {{7,13,19},{12,18,20},{16,17,19},{11,14,18},{13,15,18},{9,14,16},{1,15,17},{10,16,20},{6,11,19},{8,12,17},{4,9,13},{2,10,15},{1,5,11},{4,6,20},{5,7,12},{3,6,8},{3,7,10},{2,4,5},{1,9,3},{2,8,14}};
	int validMoves[20][3] = {{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0}};
	int sidesVisited[20]= {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	//cout << nearestNum(die[4],5) << endl;
	
	//cout << nearestValidNum(die[17],validMoves,18);
	cout << traverse(die, sidesVisited, validMoves, 1, 1,0);
	return 0;
}
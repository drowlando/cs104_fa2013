#ifndef _LIST_INTERFACE
#define _LIST_INTERFACE

template <class ItemType>
class ListInterface{

public:
	/** Inserts an item into the list at the specified position
	@pre	none
	@post 
	*/
	virtual void insert(int position, const ItemType& item)= 0;

	virtual  void remove(int position)=0;

	virtual void setEntry(int pos, const ItemType& item) = 0;

	virtual ItemType const & getEntry(int pos) = 0;
	
	virtual int getLength()=0; 

	virtual bool isEmpty()=0;

	

};
#endif
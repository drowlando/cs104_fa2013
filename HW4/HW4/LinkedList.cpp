#include "LinkedList.h"
#include <iostream>
#include <assert.h>


using namespace std;

template <class ItemType>
class Node<ItemType>{
private:
	ItemType data;
	Node<ItemType>* next;
	
public:
	Node();

	Node(ItemType entry);

	~Node();

	ItemType getData();

	Node<ItemType>* getNext();

	void setData(ItemType entry);

	void setNext(Node<ItemType>* nodePtr);

};
template <class ItemType>
class LinkedList :
	public ListInterface<ItemType>
{

private:

	int itemCount;
	Node<ItemType>* head;
	Node<ItemType>* LinkedList<ItemType>::getNodeAt(int position);


public:

	LinkedList(void);

	~LinkedList(void);

	void insert(int position, const ItemType& item);

	void remove(int position);

	void setEntry(int pos, const ItemType& item);

	ItemType const & getEntry(int pos);

	int getLength(); 

	bool isEmpty();

	void clear();



};







template <class ItemType>
Node<ItemType>::Node() : next(nullptr)
{

}

template <class ItemType>
Node<ItemType>::Node(ItemType entry): data(entry), next(nullptr)
{

}

template <class ItemType>
Node<ItemType>::~Node(){}

template <class ItemType>
ItemType Node<ItemType>::getData()
{
	return data;
}

template <class ItemType>
Node<ItemType>* Node<ItemType>::getNext()
{
	return next;
}

template <class ItemType> 
void Node<ItemType>::setData(ItemType entry){
	data = entry;
}

template <class ItemType>
void Node<ItemType>::setNext(Node<ItemType>* nodePtr)
{
	next = nodePtr;
}



template <class ItemType>
LinkedList<ItemType>::LinkedList(void) : head(nullptr), itemCount(0)
{

}

template <class ItemType>
LinkedList<ItemType>::~LinkedList(void)
{
	clear();

}


template <class ItemType>
void LinkedList<ItemType>::insert(int position, const ItemType& item){
	



	assert((position>=1) && (position<=itemCount+1));

	Node<ItemType>* newEntry = new Node<ItemType>(item);

	if (position == 1){
		newEntry->setNext(head);
		head = newEntry;

	}else{
		Node<ItemType>* prevPtr = getNodeAt(position-1);
		newEntry->setNext(prevPtr->getNext());
		prevPtr->setNext(newEntry);
	}
	itemCount++;
}


template <class ItemType>
void LinkedList<ItemType>::remove(int position){

	Node<ItemType>* currentPtr = nullptr;
	if(position == 1){
		currentPtr = head;
		head = head->getNext();

	}else{
		Node<ItemType>* prevPtr = getNodeAt(position-1);

		currentPtr = prevPtr->getNext();

		prevPtr->setNext(currentPtr->getNext());
	}
	currentPtr->setNext(nullptr);
	delete currentPtr;
	currentPtr = nullptr;
	itemCount--;
}

template <class ItemType>
void LinkedList<ItemType>::setEntry(int position, const ItemType& item){

	assert((position>=1) && (position<=itemCount));
	remove(position);
	insert(position,item);

}



template <class ItemType>
ItemType const & LinkedList<ItemType>::getEntry(int position){
	assert((position>=1) && (position <= itemCount));
	Node<ItemType>* temp = getNodeAt(position);
	cout << "the node is not null: " ;
	cout <<	(temp == nullptr) << endl;
	return temp->getData();
}

template <class ItemType>
Node<ItemType>* LinkedList<ItemType>::getNodeAt(int position){

	assert((position>=1) && (position <= itemCount));
	Node<ItemType>* temp = head;
	for(int i = 1; i < position; i++){
		temp = temp->getNext();
	}
	return temp;
}

template <class ItemType>
int LinkedList<ItemType>::getLength(){

	return itemCount;
}


template <class ItemType>
bool LinkedList<ItemType>::isEmpty(){

	return (itemCount==0);
}



template <class ItemType>
void LinkedList<ItemType>::clear(){
	while(!isEmpty()){
		remove(1);
	}
}

int main(){

	Node<string>* test = new Node<string>("I've got a bad feeling about this");




	return 0;
}
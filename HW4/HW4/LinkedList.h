#ifndef _LINKED_LIST
#define _LINKED_LIST

#include "listinterface.h"

template <class ItemType>
class Node<ItemType>{
private:
	ItemType data;
	Node<ItemType>* next;
	
public:
	Node();

	Node(ItemType entry);

	~Node();

	ItemType getData();

	Node<ItemType>* getNext();

	void setData(ItemType entry);

	void setNext(Node<ItemType>* nodePtr);

};


template <class ItemType>
class LinkedList :
	public ListInterface<ItemType>
{

private:

	int itemCount;
	Node<ItemType>* head;
	Node<ItemType>* LinkedList<ItemType>::getNodeAt(int position);


public:

	LinkedList(void);

	~LinkedList(void);

	void insert(int position, const ItemType& item);

	void remove(int position);

	void setEntry(int pos, const ItemType& item);

	ItemType const & getEntry(int pos);

	int getLength(); 

	bool isEmpty();

	void clear();



};
#include "LinkedList.cpp"
#endif


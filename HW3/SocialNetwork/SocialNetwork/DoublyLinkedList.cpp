#include "DoublyLinkedList.h"
#include <iostream>

template <class ItemType>
DoublyLinkedList<ItemType>::DoublyLinkedList(void)
{
	head=tail=NULL;
	size = 0;

}

template <class ItemType>
DoublyLinkedList<ItemType>::~DoublyLinkedList(void)
{
	Node<ItemType>* temp = head;
	while(temp != NULL){
		Node<ItemType>* next = temp->getNext();
		delete temp;
		temp = next;

	}
	head = tail = NULL;
}



template <class ItemType>
void DoublyLinkedList<ItemType>::add(const ItemType & newElement)
{
	cout<< "inside of DLL add function" << endl;
	Node<ItemType>* toAdd = new Node<ItemType>(newElement);

	if(getSize() == 0){
		cout << "Inside of DLL add function: adding first element" << endl;
		head=tail=toAdd;

	}else{
		cout<< "Inside of DLL add function: adding another element" << endl;
		tail->setNext(toAdd);
		toAdd->setPrev(tail);
		tail = toAdd;

	}
	size++;
	cout << "end of DLL add function" << endl;
}


template <class ItemType>
void DoublyLinkedList<ItemType>::remove(Node<ItemType>* item){
	unplugElement(item);
	delete item;
	item = nullptr;
	size--;
}


//template <class ItemType>
//bool DoublyLinkedList<ItemType>::removeByElement(const ItemType& element)
//{
//	//Node<ItemType>* toRemove = new Node<ItemType>(element);
//	Node<ItemType>* temp;
//	//bool removalSuccess = ((toRemove != NULL) && !isEmpty());
//	if (contains(element)){
//		cout << "the element is found and awaiting deletion"<< endl;
//		temp = getNodePointer(element);
//		unplugElement(temp);
//		delete temp;
//		temp = NULL;
//		size --;
//		return true;
//	}
//	return false;
//}

template <class ItemType>
void DoublyLinkedList<ItemType>::unplugElement( Node<ItemType>* elementPtr){
	if(elementPtr->getPrev() == NULL){
		head = elementPtr->getNext();
	}else{
		elementPtr->getPrev()->setNext(elementPtr->getNext());

	}
	if((elementPtr->getNext()) == NULL ){
		tail = (elementPtr->getPrev());
	}else{
		(elementPtr->getNext())->setPrev(elementPtr->getPrev()) ;
	}

}

template <class ItemType>
int DoublyLinkedList<ItemType>::getSize() const{

	return size;
}

template <class ItemType>
bool DoublyLinkedList<ItemType>::isEmpty(void)
{
	return (size == 0);
}

template <class ItemType>
bool DoublyLinkedList<ItemType>::contains(const ItemType& elementData)
{
	if((getNodePointer(elementData)) != NULL){
		return true;
	}
	return false;
}

template <class ItemType>
Node<ItemType>* DoublyLinkedList<ItemType>::getNodePointer(const ItemType& elementData)
{
	Node<ItemType>* temp = head;

	for(int i= 0; i < size;i++){
		if(elementData == temp->getData()){
			return temp;
		}else{
			temp = temp->getNext();
		}
	}



	return NULL;
}

template <class ItemType>
Node<ItemType>* DoublyLinkedList<ItemType>::getHead() const{
	return head;
}
#ifndef _USER
#define _USER
#pragma once

#include <iostream>
#include "Wall.h"

using namespace std;


class User
{
private:
	Wall userWall;
	string userName;
	string userPass;





public:
	User(void);
	User(string userInfo);
	~User(void);
	void getName(void);
	void setName(string name);
	string getPassword(string password);
	string setPassword(void);
	void setUniversity(void);
	string getUniversity(void);
	void postToWall(void);
	void deleteMessage(void);
	string userInfoToString(void);
};
#include "User.cpp"
#endif
#ifndef _USER_LIST
#define _USER_LIST


#pragma once
#include "doublylinkedlist.h"
#include "User.h"
class UserList :
	public DoublyLinkedList<User>
{
public:
	UserList(void);
	~UserList(void);
};
#include "UserList.cpp"
#endif
#ifndef _DOUBLY_LINKED_LIST
#define _DOUBLE_LINKED_LIST
#pragma once

#include "Node.h"

using namespace std;

template <class ItemType>
class DoublyLinkedList
{
private:
	int size;
	Node<ItemType>* head;
	Node<ItemType>* tail;

public:
	//constructors
	DoublyLinkedList(void);
	~DoublyLinkedList(void);
	// standard member functions
    void add(const ItemType & newElement);
	void remove(Node<ItemType>* item);
	//bool removeByElement(const ItemType& element);
	bool contains(const ItemType& element);
	bool isEmpty(void);
	int getSize() const;

	Node<ItemType>* getHead() const;
	void unplugElement( Node<ItemType>* element);	
	Node<ItemType>* getNodePointer(const ItemType& element);
};
#include "DoublyLinkedList.cpp"
#endif
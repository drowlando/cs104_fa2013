#include "Node.h"


template <class ItemType>
Node<ItemType>::Node(void)
{
	next= prev = nullptr;
	
}


//template <class ItemType>
//Node<ItemType>::~Node(void)
//{
//	delete next;
//	delete prev;
//	next = nullptr;
//	prev = nullptr;
//}


template <class ItemType>
Node<ItemType>::Node(const ItemType& item)
{
	data = item;
}

template <class ItemType>
void Node<ItemType>::setData(const ItemType&  item)
{
	data = item;
}

template <class ItemType>
void Node<ItemType>::setNext(Node<ItemType>* nextNodePtr)
{
	next = nextNodePtr;
}

template <class ItemType>
void Node<ItemType>::setPrev(Node<ItemType>* prevNodePtr)
{
	prev = prevNodePtr;
}

template <class ItemType>

ItemType Node<ItemType>::getData(void) const
{
	return data;
}

template <class ItemType>
Node<ItemType>* Node<ItemType>::getNext(void)
{
	return this->next;
}

template <class ItemType>
Node<ItemType>* Node<ItemType>::getPrev(void)
{
	return prev;
}

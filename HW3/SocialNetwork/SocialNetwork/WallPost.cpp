#include <iostream>
#include "WallPost.h"
#include <string>
#include "Wall.h"

using namespace std;

int ::WallPost::postCount= 0;

WallPost::WallPost(){
	message = "";
	author = "";
	messageID = to_string(postCount);
	postCount++;
}

WallPost::WallPost(string messagePost, string name){
	message = messagePost;
	author =  name;
	messageID = to_string(postCount);
	postCount++;
}
WallPost::WallPost(string messagePost, string name, int postNumber){
	message = messagePost;
	author =  name;
	messageID=to_string(postCount);
	postCount++;
}
WallPost::WallPost(string & stringFormat){
	if(stringFormat != ""){
		this->messageID = Helper::keySubstring(stringFormat, "#id#");
		this->message =    Helper::keySubstring(stringFormat, "$msg$");
		this->author = (Helper::keySubstring(stringFormat,"@aut@"));
		postCount++;
	}
}


WallPost::~WallPost(){
}
string WallPost::getAuthor() const {
	return author;
}
void WallPost::setAuthor(const string name){
	author = name;
}
string WallPost::getMessage() const{
	return message;
}
void WallPost::setMessage(const string messagePost){
	message = messagePost;

}
void WallPost::setID(const int number)
{
	messageID = to_string(number);
}

string WallPost::getID() const{
	return messageID;
}

string WallPost::toString(){
	string postContent = "*************************\n";
		
	postContent += message + " by " + author;

	postContent +=       "\n*************************";
	postContent +=       "\n          message#: " + messageID;
	
	return postContent;
}

	static int getPostCount(){

		return WallPost::postCount;
	}


#include "Node.h"
#include <cstddef>

using namespace std;

template<class ItemType> Node<ItemType>::Node(){
	next = NULL;
	prev = NULL;
}


template<class ItemType> Node<ItemType>::Node(const ItemType& data){
	next = prev = NULL;
	item = data;
}

template<class ItemType> Node<ItemType>::Node(const ItemType& data, Node<ItemType>* nextNodePtr){
	next=prev = NULL;
	item = data;
	next = nextNodePtr;
}
template<class ItemType> void Node<ItemType>::setData(const ItemType& data){
	item = data;

}
template<class ItemType> ItemType Node<ItemType>::getData() const{
	return item;
}
template<class ItemType> Node<ItemType>* Node<ItemType>::getNext() const{
	return next;
}
template<class ItemType> void Node<ItemType>::setNext(Node<ItemType>* nextNodePtr){
	next = nextNodePtr;
}
template<class ItemType> Node<ItemType>* Node<ItemType>::getPrev() const{
	return prev;
}
template<class ItemType> void Node<ItemType>::setPrev(Node<ItemType>* prevNodePtr){
	this.prev = prevNodePtr;
}
#ifndef _WALL
#define _WALL
#include "DoublyLinkedList.h"
#include "WallPost.h"
#include <iostream>
#include <string>



class Wall
{
private:
	DoublyLinkedList<WallPost>* postList;
	int numberOfPosts;


public:


	Wall(void);
	Wall(WallPost post);
	Wall(string wallFormat);
	~Wall(void);
	string formatToString();
	//WallPost & postFromString(string & stringFormat);
	void printAll();
	void addWallPost(const WallPost & newPost);
	bool deleteWallPost(int messageNumber);
	int getSize();

};
#include "Wall.cpp"
#endif
namespace Helper{
	string keySubstring(string & line, string key);
	WallPost postFromString(string & stringFormat);

}

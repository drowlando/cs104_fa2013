#ifndef _WALL_POST
#define _WALL_POST

#pragma once
#include <iostream>
#include <string>


class WallPost 
{
private:
	string message;
	string author;
	string messageID;
	
	


public:

	WallPost();

	WallPost(string messagePost, string author);

	WallPost(string messagePost, string author, int postNumber);

	WallPost(string & stringFormat);

	static int postCount;
	
	~WallPost();

	string getAuthor() const;

	void setAuthor(const string name);

	string getMessage() const; 

	void setMessage(const string message);

	string toString();

	string getID() const;

	void setID(const int number);

	void setMessageNumber(int number);

	static int getPostCount();

	static string subKey(string line);
};
//int WallPost::postCount= 0;
#include "WallPost.cpp"
#endif

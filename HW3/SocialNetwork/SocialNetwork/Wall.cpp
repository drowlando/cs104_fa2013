#include "Wall.h"
#include "WallPost.h"
#include "DoublyLinkedList.h"
#include <string>
#include <iostream>
#include <assert.h>

using namespace std;

Wall::Wall(void)
{
	postList = new DoublyLinkedList<WallPost>;
	numberOfPosts = 0;
}

Wall::Wall(WallPost post){
	postList = new DoublyLinkedList<WallPost>;
	postList->add(post);
	numberOfPosts = postList->getSize();

}

Wall::Wall(string wallFormat){
	postList = new DoublyLinkedList<WallPost>();
	numberOfPosts = 0;
	while(wallFormat != ""){

	}


}


Wall::~Wall(void)
{
	delete postList;
	postList = NULL;
}


void Wall::addWallPost(const WallPost & newPost)
{
	postList->add(newPost);
	numberOfPosts++;
	cout << "post has been added to the wall" << endl;
}


bool Wall::deleteWallPost(int messageNumber)
{
	Node<WallPost>* temp = postList->getHead();
	for(int i = 0 ; i<postList->getSize(); i++){
		if (temp->getData().getID() == to_string(messageNumber)){
			postList->remove(temp);
			numberOfPosts--;
			return true;
		}else{
			temp= temp->getNext();
		}
	}
	return false;
}
void Wall::printAll(){
	Node<WallPost>* temp = postList->getHead();
	while(temp != nullptr){
		cout<< temp->getData().toString() << endl;
		temp = temp->getNext();
	}

}


string Wall::formatToString(){
	Node<WallPost>* temp = postList->getHead();
	string format = "";
	while(temp != nullptr){
		format += temp->getData().getID() + "#id#";
		format += temp->getData().getMessage() + "$msg$";
		format += temp->getData().getAuthor() + "@aut@";
		temp = temp->getNext();
	}
	return format;
}

int Wall::getSize(){
	return numberOfPosts;
}


string Helper::keySubstring(string & line, string key){
	string output= "";
	for (string::iterator itr = line.begin(); itr!=line.end()/*-key.size()+1*/;itr++){
		if (string(itr,itr+key.size())==key){
			output = string(line.begin(),itr);
			line = string (itr+key.size(),line.end());
			cout << "The line currently: " << line << endl;
			return output;
		}
		
	}
	//this function should throw an exception here
	cout << "That key does not appear in the line" << endl;
	return line;
}
WallPost Helper::postFromString(string & stringFormat){
	WallPost newPost = WallPost();
	if(stringFormat != ""){
		newPost.setID(stoi(Helper::keySubstring(stringFormat, "#id#")));
		newPost.setMessage(Helper::keySubstring(stringFormat, "$msg$"));
		newPost.setAuthor(Helper::keySubstring(stringFormat,"@aut@"));
	}
	return newPost;
}
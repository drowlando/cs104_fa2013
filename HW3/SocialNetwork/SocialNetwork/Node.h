#ifndef _NODE
#define _NODE

template <class ItemType>
class Node
{

private:
	ItemType data;
	Node<ItemType>* next;
	Node<ItemType>* prev;
public:
	Node(void);
	//~Node(void);
	Node(const ItemType& item);
	void setData(const ItemType&  item);
	void setNext(Node<ItemType>* nextNodePtr);
	void setPrev(Node<ItemType>* prevNodePtr);
	ItemType getData(void) const;
	Node<ItemType>* getNext(void);
	Node<ItemType>* getPrev(void);

	
};
#include "Node.cpp"
#endif

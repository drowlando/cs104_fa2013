#ifndef _WALL_POST
#define _WALL_POST

#include <iostream>



class WallPost 
{
private:
	string message;
	string author;
	string messageID;
	
	


public:

	WallPost();

	WallPost(string messagePost, string author);

	WallPost(string messagePost, string author, int postNumber);

	WallPost(string & stringFormat);

	static int postCount;
	
	~WallPost();

	string getAuthor() const;

	void setAuthor( string name);

	string getMessage() const; 

	void setMessage( string message);

	string toString();

	string getID() const;

	void setID( int number);

	void setMessageNumber(int number);

	static int getPostCount();

	string keySubstring(string & line, string key);

	string toStringFormat();
};

#include "WallPost.cpp"
#endif

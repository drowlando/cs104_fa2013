#ifndef _USER
#define _USER


#include <iostream>
#include "Wall.h"
#include <set>
#include <vector>
#include "UserName.h"

using namespace std;

class User;
/*
class Friend{
private:
	UserName uName;
	string realName;
	User* vertex;
	bool areFriends;// used to handle pending friend requests


public:
	Friend();
	Friend(string name);
	Friend(string name, bool friends);
	Friend(const Friend& other);
	~Friend();
	bool getFriendShip();
	string getName();
	void denyRequest();
	void acceptRequest();
	void unfriend();
	void requestFriendship(string message);
	void setAreFriends(bool status );
	void setVertex(User* user);
	Friend & operator=(const Friend & other);
	void sendMessage(string message);


};//end Friend
*/

//begin User
class User
{

	friend class Friend;

private:

	Wall userWall;
	UserName uName;
	string realName;
	string userPass;
	string university;
	//LinkedList<Friend> friendList;
	//string friendFormatString();
	//void buildFriendList(LinkedList<Friend> & list, string & format);

public:


	User(string userName, string realName, string pass);
	User(string userName, string realName, string pass, string school);
	User(string format);
	User( const User & other);
	User();
	~User(void);

	string getRealName(void);
	void setRealName(string const name);
	string getUserNameStr();
	UserName & getUserNameObj();
	string  getPassword();
	void setPassword(string const pass);
	void setUniversity(string const school);
	string getUniversity(void);

	void postToWall(string const message, string const author);
	bool deleteMessage(int messageNumber);
	string const toFormat(void);
	string keySubstring(string & line, string key);
	string toString();
	User & operator=(const User & other);

	int getWallSize();

	void addFriend(const Friend & person);
	int numberOfFriends();





};
#include "User.cpp"
#endif
#include "Hashtable.h"
#include <iostream>
#include <vector>
using namespace std;



template <class KeyType, class ValueType>
Hashtable<KeyType,ValueType>::Hashtable(int initialCapacity){
	itemList = std::vector<vector<Node<KeyType,ValueType>>>(initialCapacity);
	capacity = initialCapacity;
	size = 0;
}

template <class KeyType, class ValueType>
Hashtable<KeyType,ValueType>::Hashtable(){
	itemList = std::vector<vector<Node<KeyType,ValueType>>>(201);
	capacity = 201;
	size = 0;
}

template <class KeyType, class ValueType>
void Hashtable<KeyType,ValueType>::add(const KeyType & key, const ValueType & value){
	Node<KeyType,ValueType> newNode;
	newNode.key = key;
	newNode.value = value;
	//int index = key.hash() % itemList.size();
	
	if(getLoad()>0.5) rehashAll();
	//if(typeid(KeyType) == string){
	//	int index = hash(key) % itemList.size();
	//	itemList[index].push_back(newNode);
	//	size+=1;
	//}else{

	itemList[key.hash() % itemList.size()].push_back(newNode);
	size+=1;
	
}

template <class KeyType, class ValueType>
ValueType & Hashtable<KeyType,ValueType>::get (const KeyType & key) const{
	int index = key.hash()%itemList.size();
	ValueType outPut;
	for (auto itr = itemList[index].begin();itr<itemList[index].end();itr++){
		if(key == (*itr).key){
			outPut = (itemList[index].front().value);
			return outPut;
		}
	}
	return outPut;
}

template <class KeyType,class ValueType>
bool Hashtable<KeyType,ValueType>::remove(const KeyType & key){
	int index = key.hash() % itemList.size();
	if (itemList[index].empty()){
		return false;
	}
	for(auto itr = itemList[index].begin();itr!=itemList[index].end();itr++){
		if((*itr).key == key){
			itemList[index].erase(itr);
			size-=1;
			return true;
		}
	}
	return false;
}

template <class KeyType,class ValueType>
void Hashtable<KeyType, ValueType>::rehashAll(){
	vector<vector<Node<KeyType,ValueType>>> oldList = (itemList);
	//vector<vector<Node<KeyType,ValueType>>> newList = vector<vector<Node<KeyType,ValueType>>>(capacity*2);
	itemList = vector<vector<Node<KeyType,ValueType>>>(capacity*2);
	capacity *=2;
	size = 0;
	for(auto subVec = oldList.begin();subVec!=oldList.end();subVec++){
		for(auto itr = (*subVec).begin();itr!=(*subVec).end();itr++){
			this->add((*itr).key,(*itr).value);
		}
	}
	//oldList.clear();
	//oldList.shrink_to_fit();

}

template<class KeyType,class ValueType>
void Hashtable<KeyType,ValueType>::printAllKeys(){
	for(auto subVec = itemList.begin();subVec!=itemList.end();subVec++){
		for(auto itr = (*subVec).begin();itr!=(*subVec).end();itr++){
			//cout << (*itr).key.toString() << ".............. " << (*itr).value << endl; //TODO add try/catch -> throw exception when KeyType does not have toString() function
			cout << (*itr).key.toString() << endl;
		}

	}
}

template<class KeyType,class ValueType>
int Hashtable<KeyType,ValueType>::getSize(){
	return size;
}

template<class KeyType,class ValueType>
int Hashtable<KeyType,ValueType>::getCapacity(){
	return capacity;
}

template<class KeyType,class ValueType>
double Hashtable<KeyType,ValueType>::getLoad(){
	return static_cast<double>(size)/static_cast<double>(capacity);
}
template<class KeyType,class ValueType>
int Hashtable<KeyType,ValueType>::hash(string aString){
	int hashNumber= 0;
	for(int i = 0; i < userName.length();i++){
		hashNumber = hashNumber + (userName[i]*(2**(i+1)));
	}
	return hashNumber;
}


template <class KeyType, class ValueType>
typename Hashtable<KeyType,ValueType>::Iterator Hashtable<KeyType,ValueType>::begin(){
	//Node<KeyType,ValueType>* firstElement = &(*((*itemList.begin()).begin()));
	Iterator beginIterator = Iterator(); 
	beginIterator.client = &itemList;
	beginIterator.mainVectorItr = itemList.begin();
	beginIterator.mainVector_endItr = itemList.end();
	if(beginIterator.mainVectorItr == beginIterator.mainVector_endItr){
		//beginIterator.subVectorItr = itemList.end();
		//beginIterator.subVector_endItr = itemList.end();
		beginIterator.currentNode = nullptr;
	}else{
	beginIterator.subVectorItr = (*beginIterator.mainVectorItr).begin();
	beginIterator.subVector_endItr = (*beginIterator.mainVectorItr).end();
	beginIterator.currentNode = &(*beginIterator.subVectorItr);
	}

	return beginIterator;
}

template <class KeyType, class ValueType>
typename Hashtable<KeyType,ValueType>::Iterator Hashtable<KeyType,ValueType>::end(){
	Iterator endIterator = Iterator();
	endIterator.client = *itemList;
	endIterator.mainVectorItr = itemList.end();
	endIterator.mainVector_endItr = endIterator.mainVectorItr;
	endIterator.subVectorItr = (*(--itemList.end())).end();
	endIterator.subVector_endItr = endIterator.subVectorItr;
	endIterator.currentNode = nullptr;
	return endIterator;
}



//ITERATOR STUFF 


template<class KeyType,class ValueType>
Hashtable<KeyType,ValueType>::Iterator::Iterator():
	currentNode(nullptr),client(nullptr),mainVector_endItr(),subVectorItr(),subVector_endItr()
{
}

template<class KeyType,class ValueType>
Hashtable<KeyType,ValueType>::Iterator::Iterator(Hashtable<KeyType,ValueType>* hashTablePtr):client(hashTablePtr)
{
	currentNode = &(*((*itemList.begin()).begin()));
	mainVectorItr = itemList.begin();
	mainVector_endItr = itemList.end();
	subVector = (*mainVectorItr).begin();
	subVector_endItr =(*mainVectorItr).end();
}



template <class KeyType,class ValueType>
typename Hashtable<KeyType,ValueType>::Iterator & Hashtable<KeyType,ValueType>::Iterator::operator++(){
	if((mainVectorItr == mainVector_endItr) && (subvectorItr == subVector_endItr)

	return *this;
}

template <class KeyType,class ValueType>
ValueType & Hashtable<KeyType,ValueType>::Iterator::operator*(){
	return currentNode->value;
}

//template <class KeyType, class ValueType>
//Hashtable<KeyType,ValueType>::Node<KeyType,ValueType>::Node<KeyType,ValueType>():key(),value(){
//
//
//}

//template <class KeyType, class ValueType>
//Node<KeyType,ValueType> & Node<KeyType,ValueType>::operator=(const typename Hashtable<KeyType,ValueType>::Node<KeyType,ValueType> & other){
//	Node<KeyType,ValueType> newNode;
//	newNode.key = other.key;
//	newNode.value = other.value;
//	return *newNode;
//}

//template <class KeyType, class ValueType>
//Hashtable<KeyType,ValueType>::Node<KeyType,ValueType>::Node(const Node<KeyType,ValueType> & other){
//	this->key = other.key;
//	this->value = other.value;
//}
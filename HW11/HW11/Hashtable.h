#ifndef _HASHTABLE
#define _HASHTABLE
#include <iostream>
#include <vector>
#include <string>






template <class KeyType, class ValueType>
class Hashtable{


private:
	// Begin Node
	template <class KeyType, class ValueType>
	struct Node {
		Node(){
			KeyType key;
			ValueType value;
		}
		Node(KeyType key,ValueType value){
			this->key = key;
			this->value = value;
		}
		Node(const Node<KeyType,ValueType> & other){
			this->key = other.key;
			this->value = other.value;
		}
		//Node<KeyType,ValueType> & operator=(Node<KeyType,ValueType> & other);
		KeyType key;
		ValueType value;
	};//END Node

	vector<vector<Node<KeyType,ValueType>>> itemList;
	int capacity;//the # of size_of ValueType capacity initially assigned to the vector 
	int size;// the number of items in the hash table
	void rehashAll();

public:
	// Begin Iterator
	class Iterator:private std::iterator<bidirectional_iterator_tag,Node<KeyType,ValueType>>{
		friend class Hashtable<KeyType,ValueType>;
	public:
		Iterator();
		Iterator( Hashtable<KeyType,ValueType>* hashTablePtr);
		Iterator & operator++();
		Iterator operator++(int);//post increment;
		ValueType & operator*();

		Iterator & operator--();

		Iterator & operator-(int num)  ;

		Iterator & operator+(int num);
		


	protected:
		Node<KeyType,ValueType>* currentNode;
		vector<vector<Node<KeyType,ValueType>>>* client;
		typename vector<vector<Node<KeyType,ValueType>>>::iterator mainVectorItr;
		typename vector<vector<Node<KeyType,ValueType>>>::iterator mainVector_endItr;// mainVector.end()
		typename vector<Node<KeyType,ValueType>>::iterator subVectorItr;
		typename vector<Node<KeyType,ValueType>>::iterator subVector_endItr;// subVector.end()

	};// End of Iterator





	void add (const KeyType & key, const ValueType & value);
	bool remove(const KeyType & key);
	ValueType & get (const KeyType & key) const;
	Hashtable(int initialCapacity);
	Hashtable();
	// assumes that each KeyType contains function: string toString();
	void printAllKeys();
	int getCapacity();
	int getSize();
	double getLoad();
	int hash(string aString);
	Iterator begin();
	Iterator end();

	//Hashtable<KeyType,ValueType> & const Hashtable








};
#include "Hashtable.cpp"
#endif












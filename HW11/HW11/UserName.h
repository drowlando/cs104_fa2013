#ifndef _USER_NAME
#define _USER_NAME

#include <string>
#include <iostream>

class UserName {

public:
	
	UserName(std::string someString);
	UserName(UserName & other);
	UserName();
	~UserName();
	UserName & operator=(std::string someString);
	UserName & operator=(const UserName & other);
	string operator+(string someString);
	std::string getName();
    int hash() const;
private:
	std::string data;

};

#include "UserName.cpp"
#endif
#ifndef _USER_LIST
#define _USER_LIST
#include <iostream>
#include "User.h"
#include "LinkedList.h"
#include <fstream>
#include <list>

class UserList
{
private:

	list<User> users;
	string fileName;
	User* currentUser;
	void addUser(User &  u);


public:
	UserList(void);
	UserList(string fileName);
	UserList(const UserList & other);
	~UserList(void);

	
	bool addUser(string username, string pass,string school);
	void deleteUser(string name);
	void writeToFile();
	void writeToFile(string fName);
	void readFromFile();
	void readFromFile(string fName);
	void setFileName(string fName);
	string getFileName();
	string keySubstring(string & line, string key);
	void printAll();
	UserList & operator=(const UserList & other);
	int getSize();
	bool logIn(string userName, string pass);
	void logOut();
	User* findUser(string userName);
	string getCurrentUserName();
	void printCurrentUser();
	void messageCurrentUser(string message);
	bool removeCurrentUserMessage(int messageId);
	void setCurrentUserSchool(string school);
	bool stringIsGreater(string left, string right, bool equal);
	LinkedList<User*> & searchList(string name);



};
#include "UserList.cpp"
#endif

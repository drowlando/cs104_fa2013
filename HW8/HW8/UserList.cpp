#include <iostream>
#include "UserList.h"
#include <fstream>



using namespace std;

UserList::UserList(void):fileName(){
	users = list<User>();


}


UserList::UserList(string fileName)
{
	users = list<User>();
	this->fileName = fileName;
	readFromFile();

}

UserList::UserList(const UserList & other){
	fileName = other.fileName;
	users = list<User>(users);
}

UserList::~UserList(void)
{
}

void UserList::addUser( User & u){
	users.push_front(u);
	cout << "&&&INSERTION INTO USERLIST COMP&&&" << endl;
}

bool UserList::addUser(string userName, string pass, string school){
	User* temp = findUser(userName);
	if(temp == nullptr){
		addUser(User(userName,pass, school));
		return true;
	}else{
		cout << "That username is already taken." << endl;
		return false;
	}


}

void UserList::deleteUser(string name){
	int position = 0;
	for (auto itr = users.begin();itr!=users.end();itr++){
		if((*itr).getName() == name){
			users.erase(itr);
		}else{
			position ++;
		}
	}
}

void UserList::writeToFile(){

	ofstream outFile(fileName);
	string outPut = "";
	int test = 0;
	for(auto itr = users.begin();itr!=users.end();itr++){
		cout << "Times Through: " << test << endl;
		outPut = (*itr).toFormat()+"uU%Uu"+outPut;

	}
	outFile<<outPut;
}

void UserList::writeToFile(string fName){
	this->fileName = fName;
	ofstream outFile(fileName);
	string outPut = "";
	int test = 1;
	for(auto itr = users.begin();itr!=users.end();itr++){

		outPut = (*itr).toFormat()+"uU%Uu"+outPut;
		cout << outPut << endl;
		test++;
	}
	outFile<<outPut;
}

void UserList::readFromFile(){
	readFromFile(fileName);
}

void UserList::readFromFile(string fName){
	ifstream inFile(fName);
	string temp;
	string format;
	while(inFile.good()){
		getline(inFile,temp);
		format += temp;
	}


	while(format!=""){

		addUser(User(keySubstring(format,"uU%Uu")));
	}
}

string UserList::keySubstring(string & line, string key){
	string output= "";
	for (string::iterator itr = line.begin(); itr!=line.end()/*-key.size()+1*/;itr++){
		if (string(itr,itr+key.size())==key){
			output = string(line.begin(),itr);
			line = string (itr+key.size(),line.end());

			return output;
		}

	}
	cout << "That key does not appear in the line" << endl;
	return line;
}

void UserList::printAll() {
	for (auto itr=users.begin(); itr!=users.end();itr++){
		cout <<"The size of the Wall: " << (*itr).getWallSize() << endl;
		cout << (*itr).toString() << endl;

	}
}

UserList & UserList::operator=(const UserList & other){
	if(this != &other){
		fileName = other.fileName;
		users = list<User>(users);
	}
	return *this;

}

string UserList::getFileName(){
	return fileName;
}

int UserList::getSize(){
	return users.size();
}

User* UserList::findUser(string userName){
	for(auto itr = users.begin(); itr != users.end(); itr++){
		if((*itr).getName() == userName){
			return &(*itr);
		}
	}
	return nullptr;
}

bool UserList::logIn(string userName, string pass){
	User* temp = findUser(userName);
	if(temp == nullptr) {
		cout << "No such user exists" << endl;
		return false;
	}else if(temp->getPassword() != pass){
		cout<< "Incorrect Password" << endl;
		return false;
	}else{
		cout << "Welcome to Fakebook, " << userName << endl;
		currentUser = temp;
		return true;
	}

}

void UserList::logOut(){
	currentUser = nullptr;

}

string UserList::getCurrentUserName(){
	if(currentUser == nullptr){
		cout << "No user is currently logged in" << endl;
		return "";
	}
	return currentUser->getName();
}

void UserList::printCurrentUser(){
	if(currentUser == nullptr){
		cout << "There is no logged in User at this time" << endl;
	}else{
		cout << currentUser->toString() << endl;
	}

}

void UserList::messageCurrentUser(string message){
	currentUser->postToWall(message,currentUser->getName());

}
bool UserList::removeCurrentUserMessage(int messageId){

	return(currentUser->deleteMessage(messageId));

}

void UserList::setCurrentUserSchool(string school){
	if(currentUser == nullptr){
		cout << "There is no user logged in at this time" << endl;
	}else{
		currentUser->setUniversity(school);
	}

}

bool UserList::stringIsGreater(string left, string right, bool equal){
	if(left.empty() && right.empty() && equal){
		return false;// the left and right are equal, therefore left is not greater
	}else if(left.empty() && !right.empty() && equal){
		return false; // the left is shorter and they are both equal up to this character, therefore the left is "lessthan"
	}else if(!left.empty() && right.empty() && equal){
		return true; // the right is shorter and they are both greater to this character, therefore the left is "greater than"
	}else if(left.at(0) < right.at(0)){
		return false;
	}else if ( left.at(0) > right.at(0)){
		return true;
	}else{// the two front letter are equal, check the next.
		equal = left.at(0) == right.at(0);
		left.erase(0);
		right.erase(0);
		return true && stringIsGreater(left,right,equal);
	}

}

static string toLower(string bigBritches){
	string littleBritches = "";
	for(int i = 0 ; i < bigBritches.length(); i++){
		littleBritches += tolower(bigBritches.at(i));

	}
	return littleBritches;

}

static	bool contains(string in, string contains){
	if(in.length()>contains.length()) return false; // a longer string cannot be contained in a shorter one
	string tempIn = toLower(in);
	string tempCon = toLower(contains);
	for (string::iterator itr = tempCon.begin(); itr!=tempCon.end()-tempIn.length()+1;itr++){
		if (string(itr,itr+tempIn.size())==tempIn){
			return true;
		}
	}		
	return false;

}	



LinkedList<User*> & UserList::searchList(string name){
	LinkedList<User*> temp = LinkedList<User*>();
	for(auto itr=users.begin();itr!=users.end();itr++){
		if(contains(name,(*itr).getName())){// if the searched name is capable of being contained in the word

			temp.insert(0,&(*itr));
		}

	}
	return temp;

}

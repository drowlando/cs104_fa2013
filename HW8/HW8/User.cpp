#include "User.h"
#include <iostream>

using namespace std;



User::User() {

}

User::User(string format){
	friendList = LinkedList<Friend>();
	buildFriendList(friendList, keySubstring(format,"#@#FL#@#"));
	university = keySubstring(format,"*uUsC*");
	userPass = keySubstring(format,"*uPz*");
	userName = keySubstring(format,"*uNm*");
	userWall = Wall(format);

}


User::User(string name,string pass)
{
	userName = name;
	userPass = pass;
	Wall userWall = Wall();
	friendList = LinkedList<Friend>(); 
}

User::User(string name,string pass, string school):
	userName(name), 
	userPass(pass),
	university(school)
{
	userWall = Wall();
	friendList = LinkedList<Friend>(); 
}

User::User( const User & other){
	userWall = Wall(other.userWall);
	userName = other.userName;
	userPass = other.userPass;
	university = other.university;
	friendList = LinkedList<Friend>(other.friendList);


}

User::~User(void)
{

}

User & User::operator=(const User & other){

	userWall = Wall(other.userWall);
	userName = other.userName;
	userPass = other.userPass;
	university = other.university;
	friendList = LinkedList<Friend>(other.friendList);

	return *this;

}

string  User::getName(void)
{
	return userName;
}


void User::setName(string const name)
{
	userName = name;
}


string  User::getPassword()
{
	return userPass;
}


void User::setPassword(string const pass)
{
	userPass = pass;
}


void User::setUniversity(string const school)
{
	university = school;
}


string User::getUniversity(void)
{
	return university;
}


void User::postToWall(string const message, string const author)
{

	userWall.addWallPost(WallPost(message,author));
}


bool User::deleteMessage(int messageNumber)
{
	return(userWall.deleteWallPost(messageNumber)); 
}


string const User::toFormat(void)
{
	string format = ""; 

	format = userName + "*uNm*"+format;
	format = userPass + "*uPz*"+format;
	format = university + "*uUsC*"+format;
	format = friendFormatString() + format;
	format += userWall.formatToString();
	return format;
}


string User::keySubstring(string & line, string key){
	string output= "";
	for (string::iterator itr = line.begin(); itr!=line.end()/*-key.size()+1*/;itr++){
		if (string(itr,itr+key.size())==key){
			output = string(line.begin(),itr);
			line = string (itr+key.size(),line.end());
			//cout << "The line currently: " << line << endl;
			return output;
		}

	}
	//this function should throw an exception here
	cout << "That key does not appear in the line" << endl;
	return line;
}

string User::toString(){
	string output = "*********************************\n";
	output += "Username: " + getName()+"\n";
	output+= "Password: " + getPassword() + "\n";
	output += "University: " + getUniversity() + "\n";
	cout << "The WAll of this user is empty: " << (userWall.getSize() ==0) << endl;
	output += userWall.toString();

	output +=       "\n*********************************";


	return output;
}

// TODO: delete this method when TS is complete
int User::getWallSize(){
	return userWall.getSize();

}

string   User::friendFormatString(){
	Friend temp;
	string format = "";
	for(auto itr = friendList.end()-1;itr!=friendList.begin()-1;--itr){

		format += (*itr).getName() + "$%Fn%#";

	}

	return format + "#@#FL#@#";
}

void User::buildFriendList(LinkedList<Friend> & list, string & format){
	while(format.size() != 0){
		//temp = Friend(keySubstring(format,"$%Fn%#"));
		this->addFriend(Friend(keySubstring(format,"$%Fn%#")));
	}
	cout <<"look we made lots of friends: " << list.getSize() << endl;
	for (auto itr = list.begin();itr != list.end();++itr){
		cout << (*itr).getName() << endl;
	}
	
}

void User::addFriend(const Friend & person){
	friendList.insert(0,person);

}

int User::numberOfFriends(){

	return friendList.getSize();
}





Friend::Friend():vertex(nullptr)
{
}

Friend::Friend(string name) :userName(name),vertex(nullptr),areFriends(false)
{
}

Friend::Friend(string name,bool friends):userName(name),areFriends(friends),vertex(nullptr)
{
}

Friend::Friend(const Friend& other){
	this->vertex = other.vertex;
	this->userName = other.userName;
	this->areFriends= other.areFriends;


}

Friend & Friend::operator=(const Friend& other){
	
		this->vertex = other.vertex;
		this->userName = other.userName;
		this->areFriends= other.areFriends;
	
	return *this;
}

Friend::~Friend()
{
	
}

string Friend::getName(){
	return userName;
}

void Friend::setAreFriends(bool status){
	areFriends = status;
}

void Friend::sendMessage(string message){
	vertex->postToWall(message,this->getName());


}
void Friend::acceptRequest(){
	this->areFriends = true;
}

//void Friend::requestFriendship(string message,User* vert){
//	Friend req(message,false);
//	req.vertex= *this;
//
//}
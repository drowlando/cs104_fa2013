#ifndef _LINKED_LIST
#define _LINKED_LIST
#include <iostream>
#include <iterator>

//using namespace std;

template <class T> class LinkedList;

template <class T>
class Node{
	friend class LinkedList<T>;
	Node();
	Node(  const T& entry);
	Node(const Node<T> & other);
	T data;
	Node<T>* next;
	//Node<T>* prev;
	


};//end Node
//begin LinkedList

template <class T>
class LinkedList 
{

private:


	int itemCount;
	Node<T>* head;
	//Node<T>* tail;
	Node<T>* LinkedList<T>::getNodeAt(int position);


public:

	//begin Iterator

	class Iterator : public std::iterator<std::bidirectional_iterator_tag, T>
	{
	public:
		
		Iterator();

		Iterator(Node<T>* aNode, LinkedList<T>* client);

		bool operator!=( Iterator   & other);

		bool operator==( Iterator   & other);

		Iterator & operator++();

		Iterator operator++(int);

		Iterator & operator--();

		Iterator & operator-(int num)  ;

		Iterator & operator+(int num);

		T & operator*();

	private:
		LinkedList<T>* client;
		Node<T> * node;
		Node<T> * prev;// the previously accessed node.



	};//end class Iterator

	LinkedList(void);

	LinkedList(  const LinkedList<T> & other);

	~LinkedList(void);

	void insert(int position, const  T &   item);

	void remove(int pos);

	void setEntry(int pos, const  T & item);

	T   & getEntry(int pos);

	int getSize(); 

	bool isEmpty();

	void clear();

	LinkedList<T> & operator=( const LinkedList<T> & other);

	Node<T>* getHead()  ;

	Node<T>* getTail()  ;

	Iterator   begin()  ;

	Iterator end()  ;

	bool operator==(const LinkedList<T> & other);

};//end of LinkedList
#include "LinkedList.cpp"
#endif


#ifndef _USER
#define _USER


#include <iostream>
#include "Wall.h"

using namespace std;


class User
{
private:

	Wall userWall;
	string userName;
	string userPass;
	string university;


public:
	
	User(string name, string pass);
	User(string name, string pass, string school);
	User(string format);
	User( const User & other);
	User();
	~User(void);

	string getName(void);
	void setName(string const name);
	string  getPassword();
	void setPassword(string const pass);
	void setUniversity(string const school);
	string getUniversity(void);

	void postToWall(string const message, string const author);
	void deleteMessage(int messageNumber);
	string const toFormat(void);
	string keySubstring(string & line, string key);
	string toString();
	User & operator=(const User & other);

	int getWallSize();
};
#include "User.cpp"
#endif
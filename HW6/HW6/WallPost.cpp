#include <iostream>
#include "WallPost.h"
#include <string>


using namespace std;

int ::WallPost::postCount= 0;

WallPost::WallPost(){
	message = "";
	author = "";
	messageID = to_string(postCount);
	postCount++;
}

WallPost::WallPost(string messagePost, string name){
	message = messagePost;
	author =  name;
	messageID = to_string(postCount);
	postCount++;
}
WallPost::WallPost(string messagePost, string name, int postNumber){
	message = messagePost;
	author =  name;
	messageID=to_string(postCount);
	postCount++;
}
WallPost::WallPost(string & stringFormat){
	if(stringFormat != ""){
		this->messageID = keySubstring(stringFormat, "#id#");
		this->message =    keySubstring(stringFormat, "$msg$");
		this->author = (keySubstring(stringFormat,"@aut@"));
		postCount++;
	}
}


WallPost::~WallPost(){
}
string WallPost::getAuthor() const {
	return author;
}
void WallPost::setAuthor(const string name){
	author = name;
}
string WallPost::getMessage() const{
	return message;
}
void WallPost::setMessage(const string messagePost){
	message = messagePost;

}
void WallPost::setID(const int number)
{
	messageID = to_string(number);
}

string WallPost::getID() const{
	return messageID;
}

string WallPost::toString(){
	string postContent = "*********************************\n";
		
	postContent += message + " from: " + author;

	postContent +=       "\n*********************************";
	postContent +=       "\n          message#: " + messageID;
	
	return postContent;
}

	static int getPostCount(){

		return WallPost::postCount;
	}

	string WallPost::keySubstring(string & line, string key){
	string output= "";
	for (string::iterator itr = line.begin(); itr!=line.end()/*-key.size()+1*/;itr++){
		if (string(itr,itr+key.size())==key){
			output = string(line.begin(),itr);
			line = string (itr+key.size(),line.end());
		//	cout << "The line currently: " << line << endl;
			return output;
		}
		
	}
	cout << "That key does not appear in the line" << endl;
	return line;
}

	string WallPost::toStringFormat(){
		string format = "";
		format += messageID + "#id#";
		format += message + "$msg$";
		format += author + "@aut@";
		
		return format;
	}
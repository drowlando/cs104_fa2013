#include "User.h"
#include <iostream>

using namespace std;



User::User() {

}

User::User(string format){
	university = keySubstring(format,"*uUsC*");
	userPass = keySubstring(format,"*uPz*");
	userName = keySubstring(format,"*uNm*");
	userWall = Wall(format);

}


User::User(string name,string pass)
{
userName = name;
userPass = pass;
Wall userWall = Wall();
}

User::User(string name,string pass, string school):
	userName(name), 
	userPass(pass),
	university(school)
{
userWall = Wall();
}

User::User( const User & other){
	userWall = Wall(userWall);
	userName = other.userName;
	userPass = other.userPass;
	university = other.university;


}

User::~User(void)
{

}

User & User::operator=(const User & other){
	
	userWall = other.userWall;
	userName = other.userName;
	userPass = other.userPass;
	university = other.university;
	
	return *this;

}

string  User::getName(void)
{
	return userName;
}


void User::setName(string const name)
{
	userName = name;
}


string  User::getPassword()
{
	return userPass;
}


void User::setPassword(string const pass)
{
	userPass = pass;
}


void User::setUniversity(string const school)
{
	university = school;
}


string User::getUniversity(void)
{
	return university;
}


void User::postToWall(string const message, string const author)
{

	userWall.addWallPost(WallPost(message,author));
}


void User::deleteMessage(int messageNumber)
{
	userWall.deleteWallPost(messageNumber);
}


string const User::toFormat(void)
{
	string format = ""; 

	format = userName + "*uNm*"+format;
	format = userPass + "*uPz*"+format;
	format = university + "*uUsC*"+format;
	format += userWall.formatToString();
	return format;
}


string User::keySubstring(string & line, string key){
	string output= "";
	for (string::iterator itr = line.begin(); itr!=line.end()/*-key.size()+1*/;itr++){
		if (string(itr,itr+key.size())==key){
			output = string(line.begin(),itr);
			line = string (itr+key.size(),line.end());
			//cout << "The line currently: " << line << endl;
			return output;
		}

	}
	//this function should throw an exception here
	cout << "That key does not appear in the line" << endl;
	return line;
}

string User::toString(){
	string output = "*********************************\n";
	output += "Username: " + getName()+"\n";
	output+= "Password: " + getPassword() + "\n";
	output += "University: " + getUniversity() + "\n";
	
	output += userWall.toString();
	output += "wally no workie, check wall toString next" ;
	output +=       "\n*********************************";
	
	
	return output;
}

// TODO: delete this method when TS is complete
int User::getWallSize(){
	return userWall.getSize();

}
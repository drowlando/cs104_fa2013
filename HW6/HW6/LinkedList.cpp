#include "LinkedList.h"

#include <iostream>
#include <assert.h>


using namespace std;
// node functions

template <class T>
Node<T>:: Node() : next(nullptr),data()
{}

template <class T>
Node<T>:: Node( const  T & entry) : next(nullptr),data(entry)
{

}

template <class T>
Node<T>:: Node(const Node<T> & other){
	this->next = other.next;
	this->data = other.data;
}


//LinkList Functions


template <class T>
LinkedList<T>::LinkedList(void) : head(nullptr),itemCount(0)
{

}

template <class T>
LinkedList<T>::LinkedList( const LinkedList<T> & other):head(nullptr),itemCount(0)
{
	Node<T>* temp = other.head;
	while(temp != NULL){
		insert(itemCount,temp->data);
	}

}

template <class T>
LinkedList<T>::~LinkedList(void)
{
	clear();

}


template <class T>
void LinkedList<T>::insert(int position,  const  T &   item){


	assert((position>=0) && (position<=itemCount));

	Node<T>*  newEntry = new Node<T>( item);

	if (position == 0){
		newEntry->next = head;

		head = newEntry;

	}else{
		Node<T>* prevPtr = getNodeAt(position-1);
		newEntry->next =(prevPtr->next);
		prevPtr->next =(newEntry);
	}
	itemCount++;
}


template <class T>
void LinkedList<T>::remove(int pos)
{
	//cout << "THE POS : " << pos << endl;
	assert((pos>=0) && (pos<itemCount));
	Node<T> * current = getNodeAt(pos);

	if (current == head) head = current->next;
	else{ 
		getNodeAt(pos-1)->next = current->next;
	}
	delete current;
	itemCount--;
	//cout << "Item Removed! There are now: " << itemCount << endl;
}

template <class T>
void LinkedList<T>::setEntry(int position,  const T& item){

	assert((position>=1) && (position<=itemCount));
	remove(position);
	insert(position,item);

}



template <class T>
T   & LinkedList<T>::getEntry(int position){

	assert((position>=0) && (position < itemCount));
	Node<T>* temp = getNodeAt(position);

	return temp->data;
}

template <class T>
Node<T>* LinkedList<T>::getNodeAt(int position){

	assert((position>=0) && (position < itemCount));
	Node<T>* temp = head;
	for(int i = 0; i < position; i++){
		temp = temp->next;
	}
	return temp;
}

template <class T>
int LinkedList<T>::getSize(){

	return itemCount;
}


template <class T>
bool LinkedList<T>::isEmpty(){

	return (itemCount==0);
}



template <class T>
void LinkedList<T>::clear(){

	while(head){
		remove(0);
	}
}

template <class T>
bool LinkedList<T>::operator==(const LinkedList<T> & other){
	bool output = true;
	if (this->getSize()!= other.getSize()){
		output = false;
	}else{
		Node<T>* current = head;
		Node<T>* ot = other.head;
		while((current !=nullptr) && (ot != nullptr) && output){
			output = (current->getData() == ot->getData());
			current =current->getNext();
			ot = ot->getNext();
		}
	}
	return output;
}

template <class T>
LinkedList<T> & LinkedList<T>::operator=( const LinkedList<T> & other){
	if (!(this == &other)){ 
		this->clear();
		Node<T>* temp = other.head;
		for(int i = 0;i<other.itemCount;i++){
			T dat = temp->data;
			insert(i,dat);
			temp = temp->next;
	}
}
return *this;

}



template <class T>
Node<T>* LinkedList<T>::getHead()  {
	return head;

}

template <class T>
Node<T>* LinkedList<T>::getTail()  {
	return getNodeAt(itemCount-1);
}

template <class T>
typename LinkedList<T>::Iterator    LinkedList<T>::begin()  {

	return Iterator(head,this);

}

template <class T>
typename LinkedList<T>::Iterator LinkedList<T>::end()  {

	return Iterator(nullptr,this);
}




//Iterator operator overloads etc

template <class T>
LinkedList<T>::Iterator::Iterator():node(nullptr),client(nullptr){}

template <class T>
LinkedList<T>::Iterator::Iterator(Node<T>* aNode, LinkedList<T>* client): 
	node(aNode), client(client)
{}

template <class T>
bool LinkedList<T>::Iterator:: operator!=( Iterator   & other){

	return ((node != other.node));

}

template <class T>
bool LinkedList<T>::Iterator:: operator==( Iterator   & other){
	return (node == other.node);
}

template <class T>
typename LinkedList<T>::Iterator & LinkedList<T>::Iterator::operator++(){

	node = node->next;
	return *this;

}

template <class T>
typename LinkedList<T>::Iterator & LinkedList<T>::Iterator::operator--(){
	Iterator temp = client->begin();
	Iterator last;
	while (temp != *this){
		last = temp;
		temp++;
	}
	this->node = last.node;
	return *this;

}

template <class T>
typename LinkedList<T>::Iterator LinkedList<T>::Iterator::operator++(int){
	Iterator current(*this);

	node = node->next;
	return current;
}

template <class T>
T & LinkedList<T>::Iterator::operator*(){
	return node->data;
}

template <class T>
typename LinkedList<T>::Iterator & LinkedList<T>::Iterator::operator+(int num){
	for (int i = 0;i<num;i++)
		node = node->next;
	return *this;
}
template <class T>
typename LinkedList<T>::Iterator & LinkedList<T>::Iterator::operator-(int num)  {
	Iterator temp(*this);
	for(int i = 0; i<num; i++){
		--temp;
	}
	this->node = temp.node;
	return *this;

}
#include <iostream>
#include "LinkedList.h"
#include <string>
#include "WallPost.h"
#include "Wall.h"
#include "User.h"
#include "UserList.h"

using namespace std;


int main(){

	cout << "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n"<<endl;
	WallPost post = WallPost("Nature's first green is gold,","Robert");
	WallPost post2 = WallPost("Her hardest hue to hold.", "Frost");
	WallPost post3 = WallPost("Her early leaf's a flower;", "Rob");
	WallPost post4 = WallPost("But only so an hour.", "Frosty");

	string p = post.toStringFormat();
	
	Wall w2;
	w2.addWallPost(WallPost(p));
	w2.addWallPost(post2);
	w2.addWallPost(post3);
	w2.addWallPost(post4);
	
	cout << w2.toString() << endl;
	cout << "&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&\n\n\n" << endl;
	string format = w2.formatToString();
	Wall wT = Wall(format);
	wT.printAll();
	cout << "\n\n\n\n"<< endl;
	/*wT.deleteWallPost(2);
	wT.printAll();*/
	wT.addWallPost(WallPost("Then leaf subsides to leaf.", "Roro"));
	wT.printAll();

	cout << "$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n testing of USER \n" << endl;

	User u("Winston Churchill","onion","Cambridge");
	u.postToWall("hello Winny ole chap!! ","Lil Kim");
	u.postToWall("Let's go to the clubs","Obama");
	u.postToWall("Let's just be friends", "The Dinosaurs");
	cout << "\n\nu to string : \n\n" << u.toString() << endl;
	string uFormat = u.toFormat();
	cout << "THe uFormat: " << uFormat << endl;
	
	User u2(uFormat);
	cout << u2.toString() << endl;

	User u3 = User("Liu Kang","friendship","Finish Him U");
	u3.postToWall("Come over here!", "Scorpion");
	u3.postToWall("Uppercut!", "Announcer");
	u3.postToWall("Feuuuldbasatann", "Raiden");
	u3.postToWall("Katana Wins","Announcer");
	cout << "\n\n\n\nU3 to string: "<< u3.toFormat() <<"\n\n\n\n" << endl;
	cout << u3.toString() << endl;

	cout << "\n\n\n%%% Testing THe USERLIST %%% " << endl;

	UserList uL = UserList("outF.dat");
	uL.addUser(u3);
	uL.addUser(u2);
	uL.addUser(u);



	cout<<"The size of the userList is: " << uL.getSize() << endl;
	uL.printAll();

	//cout << "THe filename is : " << uL.getFileName() << endl;
	//uL.writeToFile();
	//uL.readFromFile();

	//UserList uL2;
	//uL2.readFromFile("outF.dat");

	cout << "END" << endl;

	return 0;
}
#include "Wall.h"
#include "WallPost.h"
#include "LinkedList.h"
#include <string>
#include <iostream>
#include <assert.h>
#include <exception>

using namespace std;

Wall::Wall(void)
{
	postList = LinkedList<WallPost>();
	numberOfPosts = 0;
}

Wall::Wall(WallPost post){
	postList = LinkedList<WallPost>();
	postList.insert(0,post);
	numberOfPosts = 1;

}

//Wall::Wall(string wallFormat){
//	postList = new LinkedList<WallPost>();
//	numberOfPosts = 0;
//	WallPost* post;
//	while(wallFormat != ""){
//		post =new WallPost(wallFormat);
//		postList.insert(0,*post);
//		
//		post = nullptr;
//		cout << "Should be true last time through: " << (wallFormat=="") << endl;
//	}
//	numberOfPosts = postList.getSize();
//	cout << "postList.getSize(): " << postList.getSize()<< endl;
//	printAll();
//	cout << "all done here" << endl;
//
//}

Wall::Wall(string wallFormat){
	postList =  LinkedList<WallPost>();
	
	listInit(postList,wallFormat);

	numberOfPosts = postList.getSize();

	//cout << "postList.getSize(): " << postList.getSize()<< endl;
	//printAll();
	cout << "all done here" << endl;
	
}

//copy  ructor
Wall::Wall(  Wall & other){
	postList = other.postList;
	
	numberOfPosts = postList.getSize();

}


Wall::~Wall(void)
{
	cout << "Call to wall destructor" << endl;

}


void Wall::addWallPost( WallPost &   newPost)
{
	postList.insert(0,newPost);
	numberOfPosts++;
	//cout << "post has been added to the wall" << endl;
}


bool Wall::deleteWallPost(int messageNumber)
{
	int position = 0;
	for(auto itr = postList.begin();itr!=postList.end();itr++){
		if ((*itr).getID() == to_string(messageNumber)){
			postList.remove(position);
			numberOfPosts--;
			return true;
		}else{
			position+=1;
		}
	}
	return false;
}
void Wall::printAll(){
	WallPost post;
	int messageNum = 0;
	for(auto itr = postList.begin();itr!=postList.end();itr++){

		post = *itr;
		cout<<post.toString() << endl;
		
	}
}


string   Wall::formatToString(){
	WallPost post;
	string format = "";
	for(auto itr = postList.end()-1;itr!=postList.begin()-1;--itr){
		post = *itr;
		format += post.toStringFormat();

	}
	
	return format;
}

int Wall::getSize(){
	return numberOfPosts;
}

LinkedList<WallPost>&   Wall::listInit(LinkedList<WallPost>  &   posts, string & stringFormat){
	//cout << "stringFormat size(): " << stringFormat.size() << endl;
	if(stringFormat.size() == 0){
		return posts;
	}else{
		posts.insert(0,WallPost(stringFormat));
		listInit(posts,stringFormat);
	}

}

Wall & Wall::operator=( const Wall & other){
	postList.clear();
	postList = other.postList;
	
	numberOfPosts = postList.getSize();
	return *this;
}

string Wall::toString(){
	string output = "";
	for(auto itr = postList.begin();itr!=postList.end();itr++){

		output += (*itr).toString() + "\n";
	}
	return output;
}
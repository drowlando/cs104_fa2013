#ifndef _WALL
#define _WALL
#include "WallPost.h"
#include <iostream>
#include <string>
#include <list>



class Wall
{
private:
	LinkedList<WallPost> postList;
	int numberOfPosts;


public:

	Wall(   Wall & other);
	Wall(void);
	Wall(WallPost post);
	Wall(string wallFormat);
	~Wall(void);
	string toString();
	string formatToString();
	//WallPost & postFromString(string & stringFormat);
	void printAll();
	void addWallPost( WallPost &  newPost);
	bool deleteWallPost(int messageNumber);
	int getSize();
	LinkedList<WallPost>&   listInit(LinkedList<WallPost>  &   posts, string & stringFormat); 
	Wall & operator=( const  Wall & other);
};
#include "Wall.cpp"
#endif
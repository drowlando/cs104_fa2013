#include <iostream>
#include <fstream>
#include <stdlib.h>
using namespace std;
/* various declarations here */
ifstream inFile("numList.dat"); // the input file
int n; // number of elements in the array a
int m; // We'll use this for the maximum.

// function to recursively add the elements in an array of integers
int addArray(int array[],int i)
{
	if (i>=n){
		return 0;
	}
	int numb = array[i];
	i++;
	return numb + addArray(array,i);
}




int theMax(int array[], int i, int max){
	if(i > n){

		return max;
	}
	if(array[i]>=max){
		max = array[i];
	}
	return theMax(array,i++,max);
}



int main (void)
{


int next; // the int to be added to the array
int sum; // we'll use this for adding numbers
int s = 0; //index of the array


/* here goes code to read n numbers into a, probably from a file
Make sure to allocate space to a
*/
inFile >> n;

int *a = new int[n];
while (inFile >> next)
{
	a[s]=next;
	s++;
}
// the next part is the loop you are supposed to replace by recursion.
//
//
//for (int i = 0; i < n; i ++)
//sum += a[i];
//// This is the end of the loop.
//cout << sum << endl;

sum = addArray(a,0);
cout << sum << endl;


//m = a[0];
//for (int i = 1; i < n; i ++)
//if (a[i] > m) m = a[i];
// This is the end of the loop.
//cout << m << endl;
m = theMax(a,0,a[0]);
cout << m << endl;

return 0;
}

